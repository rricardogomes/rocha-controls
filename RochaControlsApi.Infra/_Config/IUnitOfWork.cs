﻿using NHibernate;

namespace RochaControlsApi.Infra._Config
{
    public interface IUnitOfWork
    {
        void BeginTransaction();
        void BindCurrentSessionContext();
        void Execute();
        ISession CurrentSession { get; }
        void Dispose();
    }
}
