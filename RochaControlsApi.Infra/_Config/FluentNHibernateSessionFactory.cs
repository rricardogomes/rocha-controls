﻿using NHibernate;

namespace RochaControlsApi.Infra._Config
{
    public class FluentNHibernateSessionFactory
    {
        public ISessionFactory CreateFactory(string connectionString)
        {
            var config = new FluentNHibernateConfig(connectionString);
            config.ConfigureCurrentSessionContextClass(CurrentSessionContextClassOption.web);

            // Uso somente em caso de emergencia.
            //config.CreateSchema();// Apaga um Banco já existente e cria baseado nas classes mapiadas.
            //config.UpdateSchema();// aplica mudanças em um Banco já existente.

            return config.BuildSessionFactory();
        }
    }
}
