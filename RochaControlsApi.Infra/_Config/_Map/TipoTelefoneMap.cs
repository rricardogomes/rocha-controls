﻿using FluentNHibernate.Mapping;
using RochaControlsApi.Domain._TipoTelefone;

namespace RochaControlsApi.Infra._Config._Map
{
    class TipoTelefoneMap : ClassMap<TipoTelefone>
    {
        public TipoTelefoneMap()
        {
            Table("tb_tipo_telefone");
            Id(x => x.Id).Column("PK_TIPO_TELEFONE").GeneratedBy.Identity();
            Map(x => x.TelefoneTipo).Column("TIPO");
        }
    }
}
