﻿using FluentNHibernate.Mapping;
using RochaControlsApi.Domain._QRCode;

namespace RochaControlsApi.Infra._Config._Map
{
    internal class CodigoQRMap : ClassMap<CodigoQR>
    {
        public CodigoQRMap()
        {
            Table("tb_codigoqr");
            Id(x => x.Id).GeneratedBy.Identity().Column("PK_QRCODE");
            Map(x => x.DataGerado).Column("DATE");
        }
    }
}
