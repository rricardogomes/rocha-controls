﻿using FluentNHibernate.Mapping;
using RochaControlsApi.Domain._Telefone;

namespace RochaControlsApi.Infra._Config._Map
{
    class TelefoneMap : ClassMap<Telefone>
    {
        public TelefoneMap()
        {
            Table("tb_telefone");
            Id(x => x.Id).Column("PK_TELEFONE").GeneratedBy.Identity();
            Map(x => x.DDD).Column("DDD");
            Map(x => x.TelefoneNumero).Column("NUMERO");
            References(x => x.Tipo).Column("FK_TIPO");
            References(x => x.Cliente).Column("FK_CLIENTE");
        }
    }
}
