﻿using FluentNHibernate.Mapping;
using RochaControlsApi.Domain._Equipamento;

namespace RochaControlsApi.Infra._Config._Map
{
    public class EquipamentoMap : ClassMap<Equipamento>
    {
        public EquipamentoMap()
        {
            Table("tb_equipamento");
            Id(x => x.Id).GeneratedBy.Identity().Column("ID");
            References(x => x.Cliente)
                .Column("FK_CLIENTE_EQUIPAMENTO")
                .ForeignKey()
                .Not.LazyLoad()
                .Not.Nullable();

            HasMany(x => x.Produto)
                .Not.LazyLoad();
        }
    }
}
