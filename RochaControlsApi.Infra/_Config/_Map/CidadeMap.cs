﻿using FluentNHibernate.Mapping;
using RochaControlsApi.Domain._Cidade;

namespace RochaControlsApi.Infra._Config._Map
{
    class CidadeMap : ClassMap<Cidade>
    {
        public CidadeMap()
        {
            Table("tb_cidade");
            Id(x => x.Id).Column("PK_CIDADE");
            Map(x => x.CidadeNome).Column("NOME_CIDADE");
            Map(x => x.Valor).Column("VALOR");
            References(x => x.Uf)
                .Fetch.Join()
                .Column("FK_UF")
                .ReadOnly();
        }
    }
}
