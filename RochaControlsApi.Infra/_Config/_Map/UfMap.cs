﻿using FluentNHibernate.Mapping;
using RochaControlsApi.Domain._Uf;

namespace RochaControlsApi.Infra._Config._Map
{
    class UfMap : ClassMap<Uf>
    {
        public UfMap()
        {
            Table("tb_uf");
            Id(x => x.Id).Column("PK_UF");
            Map(x => x.Estado).Column("ESTADO");
        }
    }
}
