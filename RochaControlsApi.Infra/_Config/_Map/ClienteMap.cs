﻿using FluentNHibernate.Mapping;
using RochaControlsApi.Domain._Cliente;

namespace RochaControlsApi.Infra._Config._Map
{
    class ClienteMap : ClassMap<Cliente>
    {
        public ClienteMap()
        {
            Table("tb_cliente");
            Id(x => x.Id).Column("PK_CLIENTE").GeneratedBy.Identity();
            Map(x => x.NomeCliente).Column("NOME_CLIENTE");
            Map(x => x.NomeContrato).Column("NOME_CONTRATO");
            Map(x => x.NumeroContrato).Column("NUMERO_CONTRATO");
            References(x => x.Endereco)
                .Column("FK_ENDERECO_CLIENTE")
                .Cascade.Delete()
                .ForeignKey()
                .Not.LazyLoad();

            HasMany(x => x.Telefones)
                .Not.LazyLoad();
        }
    }
}