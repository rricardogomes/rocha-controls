﻿using FluentNHibernate.Mapping;
using RochaControlsApi.Domain._Endereco;

namespace RochaControlsApi.Infra._Config._Map
{
    class EnderecoMap : ClassMap<Endereco>
    {
        public EnderecoMap()
        {
            Table("tb_endereco");
            Id(x => x.Id).Column("PK_ENDERECO").GeneratedBy.Identity();
            Map(x => x.Logradouro).Column("LOGRADOURO");
            Map(x => x.Complemento).Column("COMPLEMENTO");
            Map(x => x.Bairro).Column("BAIRRO");
            Map(x => x.Cep).Column("CEP");
            References(x => x.Cidade)
                .Column("FK_CIDADE")
                .ForeignKey()
                .Not.LazyLoad()
                .ReadOnly();
        }
    }
}