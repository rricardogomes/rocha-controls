﻿using FluentNHibernate.Mapping;
using RochaControlsApi.Domain._Produto;

namespace RochaControlsApi.Infra._Config._Map
{
    public class ProdutoMap : ClassMap<Produto>
    {
        public ProdutoMap()
        {
            Table("tb_cidade");
            Id(x => x.Serial).Column("PK_SERIAL").GeneratedBy.Identity();
            Map(x => x.Marca).Column("MARCA");
            Map(x => x.Modelo).Column("MODELO");
            Map(x => x.Descricao).Column("DESCRICAO");
            Map(x => x.Fabricante).Column("FABRICANTE");
            Map(x => x.Ano).Column("ANO");
            Map(x => x.Validade).Column("VALIDADE");
        }
    }
}