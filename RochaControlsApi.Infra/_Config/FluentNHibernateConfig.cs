﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using RochaControlsApi.Domain._Produto;
using RochaControlsApi.Infra._Config._Map;
using System;
using System.Collections.Generic;

namespace RochaControlsApi.Infra._Config
{
    public class FluentNHibernateConfig
    {
        private readonly FluentConfiguration _config;
        private readonly IList<IInterceptor> interceptors;
        //private const string ConnectionString = "Server=127.0.0.1;Port=3306;Database=DB_ROCHA_CONTROLS;Uid=root;Pwd=;";

        public FluentNHibernateConfig(string ConnectionString)
        {
            interceptors = new List<IInterceptor>();
            StoreConfiguration cfg = new StoreConfiguration();

            _config = Fluently.Configure()
                .Database(MySQLConfiguration.Standard.ConnectionString(ConnectionString))
                .Mappings(m => m.FluentMappings
                .AddFromAssemblyOf<ClienteMap>()
                ).Mappings(m =>
                    m.AutoMappings.Add(AutoMap.AssemblyOf<Produto>(cfg))
                );
        }

        public ISessionFactory BuildSessionFactory()
        {
            foreach (var interceptor in interceptors)
            {
                _config.ExposeConfiguration(x => x.SetInterceptor(interceptor));
            }
            return _config.BuildSessionFactory();
        }

        public void ConfigureCurrentSessionContextClass(CurrentSessionContextClassOption currentSessionContextClassOption)
        {
            _config.CurrentSessionContext(currentSessionContextClassOption.ToString());
        }

        public void CreateSchema()
        {
            _config.ExposeConfiguration(c => new SchemaExport(c).Execute(true, true, false))
            .BuildConfiguration();
        }

        public void UpdateSchema()
        {
            _config.ExposeConfiguration(c => new SchemaUpdate(c).Execute(false, true))
                .BuildConfiguration();
        }

        public void AddInterceptor(IInterceptor interceptor)
        {
            interceptors.Add(interceptor);
        }
    }

    public enum CurrentSessionContextClassOption
    {
        web,
        thread_static
    }

    public class StoreConfiguration : DefaultAutomappingConfiguration
    {
        public override bool ShouldMap(Type type)
        {
            return type.Namespace == "Storefront.Entities";
        }
    }
}
