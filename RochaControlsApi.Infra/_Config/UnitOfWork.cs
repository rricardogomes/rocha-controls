﻿using NHibernate;
using NHibernate.Context;
using System;
using System.Data;

namespace RochaControlsApi.Infra._Config
{
    public class UnitOfWork : IUnitOfWork
    {
        private ITransaction _transaction;
        public ISession CurrentSession { get; private set; }
        
        public UnitOfWork(ISession session)
        {
            CurrentSession = session;
        }

        public void BindCurrentSessionContext()
        {
            CurrentSessionContext.Bind(this.CurrentSession);
        }

        public void BeginTransaction()
        {
            _transaction = CurrentSession.BeginTransaction(IsolationLevel.ReadCommitted);
        }

        public void Execute()
        {
            try
            {
                if (_transaction.IsActive)
                {
                    _transaction.Commit();
                }
            }
            catch (Exception e)
            {
                _transaction.Rollback();
                throw e;
            }
        }

        public void Commit()
        {

            if (_transaction != null && _transaction.IsActive)
                _transaction.Commit();

        }

        public void Rollback()
        {
            if (_transaction != null && _transaction.IsActive)
                _transaction.Rollback();
        }

        public void Dispose()
        {
            if(CurrentSession != null)
            {
                CurrentSession.Dispose();
            }
        }

    }
}