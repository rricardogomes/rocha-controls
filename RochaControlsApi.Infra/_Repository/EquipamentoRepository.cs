﻿using RochaControlsApi.Domain._Equipamento;
using RochaControlsApi.Infra._Config;

namespace RochaControlsApi.Infra._Repository
{
    public class EquipamentoRepository : RepositoryBase<Equipamento>, IEquipamentoRepository
    {
        public EquipamentoRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }
    }
}
