﻿using RochaControlsApi.Domain._Cliente;
using RochaControlsApi.Infra._Config;

namespace RochaControlsApi.Infra._Repository
{
    public class ClienteRepository : RepositoryBase<Cliente>, IClienteRepository
    {
        public ClienteRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }

        public object ProcurarNumeroContrato(string NumeroContrato)
        {
            Cliente ClienteAlias = null;

            Cliente query = Session.QueryOver<Cliente>(() => ClienteAlias)
                .Where(x => x.NumeroContrato == NumeroContrato)
                .SingleOrDefault();

            return query;
        }
    }
}
