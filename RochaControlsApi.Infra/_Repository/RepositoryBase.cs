﻿using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using RochaControlsApi.Domain._Base;
using RochaControlsApi.Infra._Config;

namespace RochaControlsApi.Infra._Repository
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T>
    {
        private readonly UnitOfWork _unitOfWork;

        public RepositoryBase(IUnitOfWork unitOfWork)
        {
            _unitOfWork = (UnitOfWork)unitOfWork;
        }

        protected ISession Session { get { return _unitOfWork.CurrentSession; } }

        public void Clear()
        {
            Session.Clear();
        }

        public void Delete(T obj)
        {
            Session.Delete(obj);
        }
        
        public int ExecuteNonQuery(string cmdText)
        {
            return Session.CreateSQLQuery(cmdText).ExecuteUpdate();
        }

        public T Find(object id)
        {
            return Session.Get<T>(id);
        }

        public void Flush()
        {
            Session.Flush();
        }

        public IList<T> List(string order)
        {
            ICriteria query = Session.CreateCriteria(typeof(T));
            query.AddOrder(new Order(order, true));
            return query.List<T>();
        }

        public void Merge(object obj)
        {
            Session.Merge(obj);
        }

        public void Save(T obj)
        {
            Session.Save(obj);
        }

        public void SaveOrUpdate(T obj)
        {
            Session.SaveOrUpdate(obj);
        }

        public void Update(T obj)
        {
            Session.Update(obj);
        }

        public void UpdateClear(T obj)
        {
            Session.Clear();
            Session.Update(obj);
        }
    }
}
