﻿using RochaControlsApi.Domain._QRCode;
using RochaControlsApi.Infra._Config;
using System.Collections.Generic;

namespace RochaControlsApi.Infra._Repository
{
    public class CodigoQRRepository : RepositoryBase<CodigoQR>, ICodigoQRRepository
    {
        public CodigoQRRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }

        public IList<CodigoQR> ListarQrcodes()
        {

            IList<CodigoQR> query = Session.QueryOver<CodigoQR>()
                .OrderBy(x => x.DataGerado)
                .Asc
                .List();

            return query;
        }
    }
}
