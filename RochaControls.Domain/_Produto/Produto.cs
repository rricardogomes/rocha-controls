﻿using System;

namespace RochaControlsApi.Domain._Produto
{
    public class Produto
    {
        public virtual int Serial { get; set; }
        public virtual string Marca { get; set; }
        public virtual string Modelo { get; set; }
        public virtual string Descricao { get; set; }
        public virtual string Fabricante { get; set; }
        public virtual DateTime Ano { get; set; }
        public virtual DateTime Validade { get; set; }

        public Produto() { }

        public Produto(int serial, string marca, string modelo, string descricao, string fabricante, DateTime ano, DateTime validade)
        {
            Serial = serial;
            Marca = marca;
            Modelo = modelo;
            Descricao = descricao;
            Fabricante = fabricante;
            Ano = ano;
            Validade = validade;
        }
    }
}
