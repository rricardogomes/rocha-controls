﻿using RochaControlsApi.Domain._Base;

namespace RochaControlsApi.Domain._Produto
{
    public interface IProduto : IRepositoryBase<Produto>
    {
    }
}
