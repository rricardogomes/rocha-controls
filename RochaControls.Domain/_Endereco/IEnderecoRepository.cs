﻿using RochaControlsApi.Domain._Base;
using System;

namespace RochaControlsApi.Domain._Endereco
{
    public interface IEnderecoRepository : IRepositoryBase<Endereco>
    {
        Object ListarEndereco();
    }
}
