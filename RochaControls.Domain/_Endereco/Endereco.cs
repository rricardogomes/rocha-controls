﻿using RochaControlsApi.Domain._Cidade;

namespace RochaControlsApi.Domain._Endereco
{
    public class Endereco
    {
        public virtual int Id { get; set; }
        public virtual string Logradouro { get; set; }
        public virtual string Complemento { get; set; }
        public virtual string Bairro { get; set; }
        public virtual long Cep { get; set; }
        public virtual Cidade Cidade { get; set; }

        public Endereco() { }

        public Endereco(int id, string logradouro, string complemento, string bairro, long cep, Cidade cidade)
        {
            Id = id;
            Logradouro = logradouro;
            Complemento = complemento;
            Bairro = bairro;
            Cep = cep;
            Cidade = cidade;
        }
    }
}
