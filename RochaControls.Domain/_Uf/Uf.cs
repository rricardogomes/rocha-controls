﻿namespace RochaControlsApi.Domain._Uf
{
    public class Uf
    {
        public virtual string Id { get; set; }
        public virtual string Estado { get; set; }

        public Uf() { }

        public Uf(string id, string estado)
        {
            Id = id;
            Estado = estado;
        }
    }
}
