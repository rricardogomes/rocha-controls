﻿using RochaControlsApi.Domain._Base;

namespace RochaControlsApi.Domain._Uf
{
    public interface IUfRepository : IRepositoryBase<Uf>
    {
        object ListarUf();
    }
}
