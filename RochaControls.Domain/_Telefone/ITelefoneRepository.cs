﻿using RochaControlsApi.Domain._Base;

namespace RochaControlsApi.Domain._Telefone
{
    public interface ITelefoneRepository : IRepositoryBase<Telefone>
    {
        object ListarTelefone();
    }
}
