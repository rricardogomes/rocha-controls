﻿using RochaControlsApi.Domain._Cliente;
using RochaControlsApi.Domain._TipoTelefone;

namespace RochaControlsApi.Domain._Telefone
{
    public class Telefone
    {
        public virtual int Id { get; set; }
        public virtual int DDD { get; set; }
        public virtual string TelefoneNumero { get; set; }
        public virtual TipoTelefone Tipo { get; set; }
        public virtual Cliente Cliente { get; set; }

        public Telefone() { }

        public Telefone(int id, int dDD, string telefoneNumero, TipoTelefone tipo, Cliente cliente)
        {
            Id = id;
            DDD = dDD;
            TelefoneNumero = telefoneNumero;
            Tipo = tipo;
            Cliente = cliente;
        }
    }
}
