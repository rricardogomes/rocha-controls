﻿using RochaControlsApi.Domain._Uf;

namespace RochaControlsApi.Domain._Cidade
{
    public class Cidade
    {
        public virtual int Id { get; set; }
        public virtual string CidadeNome { get; set; }
        public virtual int Valor { get; set; }
        public virtual Uf Uf { get; set; }

        public Cidade() { }

        public Cidade(int id, string cidadeNome, int valor, Uf uf)
        {
            Id = id;
            CidadeNome = cidadeNome;
            Valor = valor;
            Uf = uf;
        }
    }
}
