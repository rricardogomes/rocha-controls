﻿using RochaControlsApi.Domain._Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace RochaControlsApi.Domain._Cidade
{
    public interface ICidadeRepository : IRepositoryBase<Cidade>
    {
        Object ListarCidades();
    }
}
