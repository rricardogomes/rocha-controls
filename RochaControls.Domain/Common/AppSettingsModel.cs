﻿namespace RochaControlsApi.Domain.Common
{
    public class AppSettingsModel
    {
        public string ConnectionString { get; set; }
    }
}
