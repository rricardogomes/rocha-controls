﻿namespace RochaControlsApi.Domain._TipoTelefone
{
    public class TipoTelefone
    {
        public virtual int Id { get; set; }
        public virtual string TelefoneTipo { get; set; }

        public TipoTelefone() { }

        public TipoTelefone(int id, string telefoneTipo)
        {
            Id = id;
            TelefoneTipo = telefoneTipo;
        }
    }
}
