﻿using RochaControlsApi.Domain._Base;
using System;

namespace RochaControlsApi.Domain._TipoTelefone
{
    public interface ITipoRepository : IRepositoryBase<TipoTelefone>
    {
        Object ListarTipoTelefone();
    }
}