﻿using System;
using System.Collections.Generic;

namespace RochaControlsApi.Domain._Base
{
    public interface IRepositoryBase<T>
    {
        void Save(T obj);
        void SaveOrUpdate(T obj);
        void Update(T obj);
        void Delete(T obj);
        void UpdateClear(T obj);
        T Find(Object id);
        IList<T> List(string order);
        //void Delete(string order);
        int ExecuteNonQuery(string cmdText);
        void Flush();
        void Merge(Object obj);
        void Clear();
    }
}
