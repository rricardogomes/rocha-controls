﻿using RochaControlsApi.Domain._Cliente;
using RochaControlsApi.Domain._Equipamento;
using System;

namespace RochaControlsApi.Domain._OrdemServico
{
    public class OrdemServico
    {
        public virtual int NumeroOrdemServico { get; protected set; }
        public virtual string TipoServico { get; set; }
        public virtual string Descricao { get; set; }
        public virtual DateTime DataAtendimento { get; set; }
        public virtual DateTime HoraInicial { get; set; }
        public virtual DateTime HoraFinal { get; set; }
        public virtual Cliente Cliente { get; set; }
        public virtual Equipamento Equipamento { get; set; }

        public OrdemServico() { }

        public OrdemServico(int numeroOrdemServico, string tipoServico, string descricao, DateTime horaInicial, DateTime horaFinal, Cliente cliente, Equipamento equipamento)
        {
            NumeroOrdemServico = numeroOrdemServico;
            TipoServico = tipoServico;
            Descricao = descricao;
            DataAtendimento = DateTime.Now;
            HoraInicial = horaInicial;
            HoraFinal = horaFinal;
            Cliente = cliente;
            Equipamento = equipamento;
        }
    }
}
