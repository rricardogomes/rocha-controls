﻿using RochaControlsApi.Domain._Base;

namespace RochaControlsApi.Domain._OrdemServico
{
    public interface IOrdemServicoRepository : IRepositoryBase<OrdemServico>
    {
    }
}
