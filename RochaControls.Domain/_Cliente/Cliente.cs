﻿using RochaControlsApi.Domain._Endereco;
using RochaControlsApi.Domain._Telefone;
using System.Collections.Generic;

namespace RochaControlsApi.Domain._Cliente
{
    public class Cliente
    {
        public virtual int Id { get; set; }
        public virtual string NomeCliente { get; set; }
        public virtual string NomeContrato { get; set; }
        public virtual string NumeroContrato { get; set; }
        public virtual Endereco Endereco { get; set; }

        public virtual IList<Telefone> Telefones { get; set; }

        public Cliente() { }

        public Cliente(int id, string nomeCliente, string nomeContrato, string numeroContrato, Endereco endereco, IList<Telefone> telefones)
        {
            Id = id;
            NomeCliente = nomeCliente;
            NomeContrato = nomeContrato;
            NumeroContrato = numeroContrato;
            Endereco = endereco;
            Telefones = telefones;
        }
    }
}
