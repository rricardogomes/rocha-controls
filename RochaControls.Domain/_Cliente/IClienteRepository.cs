﻿using RochaControlsApi.Domain._Base;
using System.Collections.Generic;

namespace RochaControlsApi.Domain._Cliente
{
    public interface IClienteRepository : IRepositoryBase<Cliente>
    {
        object ProcurarNumeroContrato(string NumeroContrato);
    }
}
