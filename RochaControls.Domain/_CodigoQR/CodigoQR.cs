﻿using System;

namespace RochaControlsApi.Domain._QRCode
{
    public class CodigoQR
    {
        public virtual int Id { get; set; }
        public virtual DateTime DataGerado { get; set; }

        public CodigoQR() { }

        public CodigoQR(int id, DateTime date)
        {
            Id = id;
            DataGerado = date;
        }

        public static CodigoQR CriarCodigoQr()
        {
            return new CodigoQR
            {
                DataGerado = DateTime.Now,
            };
        }
    }
}
