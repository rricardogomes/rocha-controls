﻿using RochaControlsApi.Domain._Base;
using System.Collections.Generic;

namespace RochaControlsApi.Domain._QRCode
{
    public interface ICodigoQRRepository : IRepositoryBase<CodigoQR>
    {
        IList<CodigoQR> ListarQrcodes();
    }
}
