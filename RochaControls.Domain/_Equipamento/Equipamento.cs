﻿using RochaControlsApi.Domain._Cliente;
using RochaControlsApi.Domain._Produto;
using System;
using System.Collections.Generic;

namespace RochaControlsApi.Domain._Equipamento
{
    public class Equipamento
    {
        public virtual int Id { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual Cliente Cliente { get; set; }
        public virtual IList<Produto> Produto { get; set; }

        public Equipamento() { }

        public Equipamento(Cliente cliente, IList<Produto> produto)
        {
            Cliente = cliente;
            Produto = produto;
            Date = DateTime.Now;
        }
    }
}
