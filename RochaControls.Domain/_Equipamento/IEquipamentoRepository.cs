﻿using RochaControlsApi.Domain._Base;

namespace RochaControlsApi.Domain._Equipamento
{
    public interface IEquipamentoRepository : IRepositoryBase<Equipamento>
    {
        
    }
}
