﻿using RochaControlsApi.AppService.Base;
using RochaControlsApi.Domain._Cliente;

namespace RochaControlsApi.AppService.ClienteService
{
    public interface IClienteAppService : IAppServiceBase<Cliente>
    {
        void CadastrarCliente(Cliente cliente);
        void AtualizarCliente(Cliente cliente);
        void DeletarCliente(Cliente cliente);
    }
}
