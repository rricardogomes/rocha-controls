﻿using RochaControlsApi.AppService.Base;
using RochaControlsApi.Domain._Cliente;
using RochaControlsApi.Infra._Config;
using System;

namespace RochaControlsApi.AppService.ClienteService
{
    public class ClienteAppService : AppServiceBase<Cliente>, IClienteAppService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IClienteRepository clienteRepository;

        public ClienteAppService(IUnitOfWork _unitOfWork, IClienteRepository _ClienteRepository)
            : base(_ClienteRepository)
        {
            unitOfWork = _unitOfWork;
            clienteRepository = _ClienteRepository;
        }

        public void AtualizarCliente(Cliente cliente)
        {
            try
            {
                unitOfWork.BeginTransaction();
                clienteRepository.Update(cliente);
                unitOfWork.Execute();
            }
            catch (Exception e) { throw e; }
        }

        public void CadastrarCliente(Cliente cliente)
        {
            try
            {
                unitOfWork.BeginTransaction();
                clienteRepository.Save(cliente);
                unitOfWork.Execute();
            }
            catch (Exception e) { throw e; }
        }

        public void DeletarCliente(Cliente cliente)
        {
            try
            {
                unitOfWork.BeginTransaction();
                clienteRepository.Delete(cliente);
                unitOfWork.Execute();
            }
            catch (Exception e) { throw e; }
        }
    }
}
