﻿using Microsoft.Extensions.Configuration;
using QRCoder;
using RochaControlsApi.AppService.Base;
using RochaControlsApi.Domain._QRCode;
using RochaControlsApi.Infra._Config;
using System;
using System.Drawing;

namespace RochaControlsApi.AppService.QRCodeService
{
    public class CodigoQRAppService : AppServiceBase<CodigoQR>, ICodigoQRAppService
    {
        private readonly IUnitOfWork _uow;
        private readonly ICodigoQRRepository QRCodeRepository;
        private readonly IConfiguration _Configuration;

        public CodigoQRAppService(IUnitOfWork _unitOfWork, ICodigoQRRepository _QRCodeRepository, IConfiguration configuration)
            : base(_QRCodeRepository)
        {
            _uow = _unitOfWork;
            QRCodeRepository = _QRCodeRepository;
            _Configuration = configuration;
        }

        public void AtualizarQRCode(CodigoQR qrcode)
        {
            _uow.BeginTransaction();
            QRCodeRepository.Update(qrcode);
            _uow.Execute();
        }

        public CodigoQR CadastraQRCode(CodigoQR qrcode)
        {
            try
            {
                _uow.BeginTransaction();
                QRCodeRepository.Save(qrcode);
                _uow.Execute();

                return qrcode;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public string GerarId()
        {
            try
            {
                CodigoQR codigoQR = CadastraQRCode(CodigoQR.CriarCodigoQr());
                return GerarQRCode(codigoQR);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public string GerarQRCode(CodigoQR codigoQR)
        {
            #region CodigoQR
            Base64QRCode.ImageType imgType = Base64QRCode.ImageType.Jpeg;
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(codigoQR.Id.ToString(), QRCodeGenerator.ECCLevel.Q);
            Base64QRCode qrCode = new Base64QRCode(qrCodeData);
            string qrCodeImageAsBase64 = qrCode.GetGraphic(20, Color.Black, Color.White, (Bitmap)Bitmap.FromFile(_Configuration["AppSettings:IconePath"]));
            string htmlPictureTag = $"<img alt=\"Embedded QR Code\" src=\"data:image/{imgType.ToString().ToLower()};base64,{qrCodeImageAsBase64}\"/>";
            #endregion

            return htmlPictureTag;
        }
    }
}
