﻿using RochaControlsApi.AppService.Base;
using RochaControlsApi.Domain._QRCode;

namespace RochaControlsApi.AppService.QRCodeService
{
    public interface ICodigoQRAppService : IAppServiceBase<CodigoQR>
    {
        CodigoQR CadastraQRCode(CodigoQR qrcode);
        void AtualizarQRCode(CodigoQR qrcode);
        string GerarQRCode(CodigoQR qrcode);
        string GerarId();
    }
}
