﻿using RochaControlsApi.AppService.Base;
using RochaControlsApi.Domain._Equipamento;
using RochaControlsApi.Infra._Config;
using System;

namespace RochaControlsApi.AppService.EquipamentoService
{
    public class EquipamentoAppService : AppServiceBase<Equipamento>, IEquipamentoAppService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IEquipamentoRepository equipamentoRepository;

        public EquipamentoAppService(IUnitOfWork _unitOfWork, IEquipamentoRepository _EquipamentoRepository)
            : base(_EquipamentoRepository)
        {
            unitOfWork = _unitOfWork;
            equipamentoRepository = _EquipamentoRepository;
        }

        public void CadastarEQuipamento(Equipamento equipamento)
        {
            try
            {
                unitOfWork.BeginTransaction();
                equipamentoRepository.Save(equipamento);
                unitOfWork.Execute();
            }
            catch (Exception e) { throw e; }
        }
    }
}
