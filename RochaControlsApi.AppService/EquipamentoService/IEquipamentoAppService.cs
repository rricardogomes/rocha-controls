﻿using RochaControlsApi.AppService.Base;
using RochaControlsApi.Domain._Equipamento;

namespace RochaControlsApi.AppService.EquipamentoService
{
    public interface IEquipamentoAppService : IAppServiceBase<Equipamento>
    {
        void CadastarEQuipamento(Equipamento equipamento);
    }
}
