﻿namespace RochaControlsApi.AppService.Base
{
    public interface IAppServiceBase<T>
    {
        T Select(object T);
        void Atualizar(T obj);
    }
}
