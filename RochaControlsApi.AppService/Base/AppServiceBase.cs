﻿using RochaControlsApi.Domain._Base;

namespace RochaControlsApi.AppService.Base
{
    public class AppServiceBase<T> : IAppServiceBase<T>
    {
        private readonly IRepositoryBase<T> _repositoryBase;

        public AppServiceBase(IRepositoryBase<T> repositoryBase)
        {
            _repositoryBase = repositoryBase;
        }

        public T Select(object id)
        {
            return _repositoryBase.Find(id);
        }

        public void Atualizar(T obj)
        {
            _repositoryBase.Update(obj);
        }
    }
}
