-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: db_rocha_controls
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_cidade`
--

CREATE database IF NOT EXISTS DB_ROCHA_CONTROLS ;
USE DB_ROCHA_CONTROLS;

DROP TABLE IF EXISTS `tb_cidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_cidade` (
  `PK_CIDADE` bigint(20) NOT NULL,
  `NOME_CIDADE` varchar(50) NOT NULL,
  `VALOR` int(11) DEFAULT NULL,
  `FK_UF` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`PK_CIDADE`),
  KEY `FK_UF_idx` (`FK_UF`),
  CONSTRAINT `FK_UF` FOREIGN KEY (`FK_UF`) REFERENCES `tb_uf` (`PK_UF`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_cidade`
--

LOCK TABLES `tb_cidade` WRITE;
/*!40000 ALTER TABLE `tb_cidade` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_cidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_cliente`
--

DROP TABLE IF EXISTS `tb_cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_cliente` (
  `PK_CLIENTE` bigint(20) NOT NULL,
  `NOME_CLIENTE` varchar(60) NOT NULL,
  `NOME_CONTRATO` varchar(100) NOT NULL,
  `NUMERO_CONTRATO` varchar(100) NOT NULL,
  `FK_ENDERECO_CLIENTE` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`PK_CLIENTE`),
  KEY `FK_ENDERECO_idx` (`FK_ENDERECO_CLIENTE`),
  CONSTRAINT `FK_ENDERECO_CLIENTE` FOREIGN KEY (`FK_ENDERECO_CLIENTE`) REFERENCES `tb_endereco` (`PK_ENDERECO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_cliente`
--

LOCK TABLES `tb_cliente` WRITE;
/*!40000 ALTER TABLE `tb_cliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_codigoqr`
--

DROP TABLE IF EXISTS `tb_codigoqr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_codigoqr` (
  `PK_QRCODE` bigint(20) NOT NULL AUTO_INCREMENT,
  `CONTEUDO` varchar(200) NOT NULL,
  `DATE` datetime NOT NULL,
  `FK_PRODUTO_QRCODE` bigint(20),
  PRIMARY KEY (`PK_QRCODE`),
  KEY `FK_PRODUTO_QRCODE_IDX` (`FK_PRODUTO_QRCODE`),
  CONSTRAINT `FK_PRODUTO_QRCODE` FOREIGN KEY (`FK_PRODUTO_QRCODE`) REFERENCES `tb_produto` (`PK_SERIAL`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_codigoqr`
--

LOCK TABLES `tb_codigoqr` WRITE;
/*!40000 ALTER TABLE `tb_codigoqr` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_codigoqr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_endereco`
--

DROP TABLE IF EXISTS `tb_endereco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_endereco` (
  `PK_ENDERECO` bigint(20) NOT NULL AUTO_INCREMENT,
  `LOGRADOURO` varchar(100) DEFAULT NULL,
  `COMPLEMENTO` varchar(100) DEFAULT NULL,
  `BAIRRO` varchar(100) DEFAULT NULL,
  `CEP` varchar(8) DEFAULT NULL,
  `FK_CIDADE` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`PK_ENDERECO`),
  KEY `FK_CIDADE_idx` (`FK_CIDADE`),
  CONSTRAINT `FK_CIDADE` FOREIGN KEY (`FK_CIDADE`) REFERENCES `tb_cidade` (`PK_CIDADE`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_endereco`
--

LOCK TABLES `tb_endereco` WRITE;
/*!40000 ALTER TABLE `tb_endereco` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_endereco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_equipamento`
--

DROP TABLE IF EXISTS `tb_equipamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_equipamento` (
  `PK_EQUIPAMENTO` int(11) NOT NULL AUTO_INCREMENT,
  `FK_CLIENTE_EQUIPAMENTO` bigint(20) NOT NULL,
  PRIMARY KEY (`PK_EQUIPAMENTO`),
  KEY `FK_CLIENTE_EQUIPAMENTO_idx` (`FK_CLIENTE_EQUIPAMENTO`),
  CONSTRAINT `FK_CLIENTE_EQUIPAMENTO` FOREIGN KEY (`FK_CLIENTE_EQUIPAMENTO`) REFERENCES `tb_cliente` (`PK_CLIENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_equipamento`
--

LOCK TABLES `tb_equipamento` WRITE;
/*!40000 ALTER TABLE `tb_equipamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_equipamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_funcionario`
--

DROP TABLE IF EXISTS `tb_funcionario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_funcionario` (
  `PK_MATRICULA` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOME_FUNCIONARIO` varchar(60) NOT NULL,
  `DATA_NASCIMENTO` date NOT NULL,
  `CPF` varchar(11) NOT NULL,
  `RG` varchar(10) NOT NULL,
  `EMAIL` varchar(200) NOT NULL,
  `FUNCAO` varchar(50) NOT NULL,
  `ESTADO_CIVIL` varchar(15) DEFAULT NULL,
  `FK_ENDERECO_FUNCIONARIO` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`PK_MATRICULA`),
  KEY `FK_ENDERECO_idx` (`FK_ENDERECO_FUNCIONARIO`),
  CONSTRAINT `FK_ENDERECO_FUNCIONARIO` FOREIGN KEY (`FK_ENDERECO_FUNCIONARIO`) REFERENCES `tb_endereco` (`PK_ENDERECO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_funcionario`
--

LOCK TABLES `tb_funcionario` WRITE;
/*!40000 ALTER TABLE `tb_funcionario` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_funcionario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_ordem_servico`
--

DROP TABLE IF EXISTS `tb_ordem_servico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_ordem_servico` (
  `PK_OS` bigint(20) NOT NULL AUTO_INCREMENT,
  `SOLICITANTE` varchar(45) NOT NULL,
  `FK_ENDERECO` bigint(20) DEFAULT NULL,
  `FK_CLIENTE` bigint(20) DEFAULT NULL,
  `FK_TELEFONE` bigint(20) DEFAULT NULL,
  `TIPO_SERVICO` varchar(45) DEFAULT NULL,
  `DESCRICAO` varchar(45) DEFAULT NULL,
  `FK_EQUIPAMENTO` bigint(20) DEFAULT NULL,
  `DATA_ATENDIMENTO` date DEFAULT NULL,
  `HORA_INICIAL_ATENDIMENTO` datetime DEFAULT NULL,
  `HORA_FINAL_ATENDIMENTO` datetime DEFAULT NULL,
  PRIMARY KEY (`PK_OS`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_ordem_servico`
--

LOCK TABLES `tb_ordem_servico` WRITE;
/*!40000 ALTER TABLE `tb_ordem_servico` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_ordem_servico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_produto`
--

DROP TABLE IF EXISTS `tb_produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_produto` (
  `PK_SERIAL` bigint(20) NOT NULL AUTO_INCREMENT,
  `MODELO` varchar(60) NOT NULL,
  `FABRICANTE` varchar(60) NOT NULL,
  `DESCRICAO` varchar(60) NOT NULL,
  `MARCA` varchar(60) NOT NULL,
  `ANO` date NOT NULL,
  `VALIDADE` datetime NOT NULL,
  PRIMARY KEY (`PK_SERIAL`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_produto`
--

LOCK TABLES `tb_produto` WRITE;
/*!40000 ALTER TABLE `tb_produto` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_produto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_telefone`
--

DROP TABLE IF EXISTS `tb_telefone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_telefone` (
  `PK_TELEFONE` bigint(20) NOT NULL,
  `DDD` bigint(2) DEFAULT NULL,
  `TELEFONE` varchar(10) DEFAULT NULL,
  `FK_CLIENTE` bigint(20) DEFAULT NULL,
  `FK_TIPO` int(10) DEFAULT NULL,
  PRIMARY KEY (`PK_TELEFONE`),
  KEY `FK_CLIENTE_idx` (`FK_CLIENTE`),
  KEY `FK_TIPO` (`FK_TIPO`),
  CONSTRAINT `FK_CLIENTE` FOREIGN KEY (`FK_CLIENTE`) REFERENCES `tb_cliente` (`PK_CLIENTE`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TIPO` FOREIGN KEY (`FK_TIPO`) REFERENCES `tb_tipo_telefone` (`PK_TIPO_TELEFONE`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_telefone`
--

LOCK TABLES `tb_telefone` WRITE;
/*!40000 ALTER TABLE `tb_telefone` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_telefone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_tipo_telefone`
--

DROP TABLE IF EXISTS `tb_tipo_telefone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_tipo_telefone` (
  `PK_TIPO_TELEFONE` int(10) NOT NULL,
  `TIPO` varchar(15) NOT NULL,
  PRIMARY KEY (`PK_TIPO_TELEFONE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_tipo_telefone`
--

LOCK TABLES `tb_tipo_telefone` WRITE;
/*!40000 ALTER TABLE `tb_tipo_telefone` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_tipo_telefone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_uf`
--

DROP TABLE IF EXISTS `tb_uf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_uf` (
  `PK_UF` varchar(2) NOT NULL,
  `ESTADO` varchar(20) NOT NULL,
  PRIMARY KEY (`PK_UF`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_uf`
--

LOCK TABLES `tb_uf` WRITE;
/*!40000 ALTER TABLE `tb_uf` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_uf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_usuario`
--

DROP TABLE IF EXISTS `tb_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_usuario` (
  `CPF` varchar(11) NOT NULL,
  `password` varchar(32) NOT NULL,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `ULTIMO_LOGIN` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_usuario`
--

LOCK TABLES `tb_usuario` WRITE;
/*!40000 ALTER TABLE `tb_usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-07 15:51:41
