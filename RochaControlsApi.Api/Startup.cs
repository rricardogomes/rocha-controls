﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NHibernate;
using RochaControlsApi.AppService.EquipamentoService;
using RochaControlsApi.AppService.QRCodeService;
using RochaControlsApi.Domain._Cliente;
using RochaControlsApi.Domain._Equipamento;
using RochaControlsApi.Domain._QRCode;
using RochaControlsApi.Domain.Common;
using RochaControlsApi.Infra._Config;
using RochaControlsApi.Infra._Repository;
using System.Linq;

namespace RochaControlsApi.Api
{
    public class Startup
    {

        public Startup(IHostingEnvironment environment)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(environment.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile("appsettings.Production.json", true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string ConnectionString = Configuration.GetConnectionString("DataBase");

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddSingleton(factory => { return new FluentNHibernateSessionFactory().CreateFactory(ConnectionString); });

            services.AddScoped(factory => factory
            .GetServices<ISessionFactory>().First()
            .OpenSession());

            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettingsModel>(appSettingsSection);

            //Cria a sessão no banco de dados ao iniciar a aplicação
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            //AppService
            services.AddTransient<ICodigoQRAppService, CodigoQRAppService>();//CodigoQR
            services.AddTransient<IEquipamentoAppService, EquipamentoAppService>();//Equipamento

            //Repository
            services.AddTransient<IClienteRepository, ClienteRepository>();//Cliente
            services.AddTransient<ICodigoQRRepository, CodigoQRRepository>();//CodigoQR
            services.AddTransient<IEquipamentoRepository, EquipamentoRepository>();//Equipamento
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors(x => x
            .AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader());

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
