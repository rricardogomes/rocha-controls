﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RochaControlsApi.AppService.EquipamentoService;
using RochaControlsApi.Domain._Equipamento;
using System;

namespace RochaControlsApi.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EquipamentoController : ControllerBase
    {
        private readonly IEquipamentoAppService _equipamentoAppService;

        public EquipamentoController(IEquipamentoAppService equipamentoAppService)
        {
            _equipamentoAppService = equipamentoAppService;
        }

        [HttpPost]
        public IActionResult CadastrarEquipamento([FromBody] dynamic dynamic)
        {
            try
            {
                Equipamento equipamento = JsonConvert.DeserializeObject<Equipamento>(dynamic) as Equipamento;

                _equipamentoAppService.CadastarEQuipamento(equipamento);

                return Ok();
            }
            catch (Exception e) { return BadRequest(e.Message); }
        }
    }
}