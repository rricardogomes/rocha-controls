﻿using Microsoft.AspNetCore.Mvc;
using RochaControlsApi.AppService.QRCodeService;
using RochaControlsApi.Domain._QRCode;
using System;
using System.Collections.Generic;

namespace RochaControlsApi.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CodigoQRController : ControllerBase
    {
        private readonly ICodigoQRAppService _QRCodeAppService;
        private readonly ICodigoQRRepository _QRCodeAppRepo;

        public CodigoQRController(ICodigoQRAppService QRCodeAppService, ICodigoQRRepository qRCodeAppRepo)
        {
            _QRCodeAppService = QRCodeAppService;
            _QRCodeAppRepo = qRCodeAppRepo;
        }

        [HttpPost]
        public IActionResult CriarCodigoQR()
        {
            try
            {
                string codigoQR = _QRCodeAppService.GerarId();

                return Ok(codigoQR);
            }
            catch (Exception e) { return BadRequest(e.Message); }
        }

        [HttpGet]
        public IActionResult RecuperarCodigoQR()
        {
            try
            {
                IList<CodigoQR> codigoQR = _QRCodeAppRepo.ListarQrcodes() as IList<CodigoQR>;

                return Ok(codigoQR);
            }
            catch (Exception e) { return BadRequest(e.Message); }
        }
    }
}