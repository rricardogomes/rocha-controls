﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RochaControlsApi.AppService.ClienteService;
using RochaControlsApi.Domain._Cliente;
using System;

namespace RochaControlsApi.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClienteController : ControllerBase
    {
        private readonly IClienteRepository _clienteRepository;
        private readonly IClienteAppService _clienteAppService;

        public ClienteController(IClienteRepository clienteRepository, IClienteAppService clienteAppService)
        {
            _clienteRepository = clienteRepository;
            _clienteAppService = clienteAppService;
        }

        // GET api/Cliente/cliente
        [HttpGet]
        [Route("cliente/{cliente}")]
        public IActionResult RecuperarCliente([FromRoute] string cliente)
        {
            try
            {
                var clienteRecuperar = _clienteRepository.Find(cliente);
                if (clienteRecuperar == null)
                {
                    return NotFound("Cliente não encontrado!");
                }
                return Ok(clienteRecuperar);
            }
            catch (Exception e) { return NotFound(e.Message); }
        }

        // GET api/Cliente/Contrato
        [HttpGet]
        [Route("contrato/{contrato}")]
        public IActionResult RecuperarContrato([FromRoute] string contrato)
        {
            try
            {
                var clienteRecuperar = _clienteRepository.ProcurarNumeroContrato(contrato);
                if (clienteRecuperar == null)
                {
                    return NotFound("Contrato não encontrado!");
                }
                return Ok(clienteRecuperar);
            }
            catch (Exception e) { return BadRequest(e.Message); }
        }

        //POST api/cliente
        [HttpPost]
        public IActionResult CadastrarCliente([FromBody] dynamic clienteJson)
        {
            try
            {
                Cliente cliente = JsonConvert.DeserializeObject<Cliente>(clienteJson.ToString()) as Cliente;

                if (string.IsNullOrEmpty(cliente.NomeCliente))
                {
                    throw new Exception("Nome do cliente Obrigatório");
                }
                if (string.IsNullOrEmpty(cliente.NomeContrato))
                {
                    throw new Exception("Nome do Contrato Obrigatório");
                }
                if (string.IsNullOrEmpty(cliente.NumeroContrato))
                {
                    throw new Exception("Número do contrato Obrigatório");
                }

                _clienteAppService.CadastrarCliente(cliente);

                return Ok();
            }
            catch (Exception e) { return BadRequest(e.Message); }
        }

        //PUT api/cliente
        [HttpPut]
        public IActionResult AtualizarCliente([FromBody] dynamic clienteJson)
        {
            try
            {
                Cliente cliente = JsonConvert.DeserializeObject<Cliente>(clienteJson.ToString()) as Cliente;

                if (string.IsNullOrEmpty(cliente.NomeCliente))
                {
                    throw new Exception("Nome do cliente Obrigatório");
                }
                if (string.IsNullOrEmpty(cliente.NomeContrato))
                {
                    throw new Exception("Nome do Contrato Obrigatório");
                }
                if (string.IsNullOrEmpty(cliente.NumeroContrato))
                {
                    throw new Exception("Número do contrato Obrigatório");
                }

                _clienteAppService.AtualizarCliente(cliente);

                return Ok();
            }
            catch (Exception e) { return BadRequest(e.Message); }
        }

        //Delete api/cliente
        [HttpDelete("{clienteId}")]
        public IActionResult DeletarCliente([FromRoute] dynamic clienteId)
        {
            try
            {
                _clienteAppService.DeletarCliente(clienteId);

                return NoContent();
            }
            catch (Exception e) { return BadRequest(e.Message); }
        }
    }
}